.. python-picassoclient documentation master file, created by
   sphinx-quickstart on Tue Dec  6 17:37:53 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to python-picassoclient's documentation!
=============================================

Contents:

.. toctree::
   :maxdepth: 2

   modules.rst



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

